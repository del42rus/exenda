<?php

namespace ContentManager\Domain\Entity;

use Core\Domain\Entity\AbstractEntity;

class Content extends AbstractEntity
{
    private $title;

    private $text;

    private $created;

    private $updated;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }
}