<?php

namespace ContentManager\Controller\Admin\Factory;

use ContentManager\Controller\Admin\ContentController;
use Interop\Container\ContainerInterface;
use ContentManager\Form\Admin\ContentForm;
use Zend\ServiceManager\Factory\FactoryInterface;

class ContentControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $contentRepository = $container->get('ContentRepository');
        $contentForm = $container->get('FormElementManager')->get(ContentForm::class);

        return new ContentController($contentRepository, $contentForm);
    }
}