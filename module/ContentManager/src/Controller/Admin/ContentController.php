<?php

namespace ContentManager\Controller\Admin;

use ContentManager\Domain\Entity\Content;
use ContentManager\Domain\Repository\ContentRepositoryInterface;
use Exception;
use ContentManager\Form\Admin\ContentForm;
use Zend\Mvc\Controller\AbstractActionController;

class ContentController extends AbstractActionController
{
    private $contentRepository;

    private $contentForm;

    public function __construct(ContentRepositoryInterface $contentRepository, ContentForm $contentForm)
    {
        $this->contentRepository = $contentRepository;
        $this->contentForm = $contentForm;
    }

    public function indexAction()
    {
        $page = $this->params()->fromRoute('page');
        $content = $this->contentRepository->paginate($page);

        return [
            'content' => $content
        ];
    }

    public function createAction()
    {
        $request = $this->getRequest();

        $content = new Content();
        $this->contentForm->bind($content);

        if ($request->isPost()) {
            $this->contentForm->setData($request->getPost());

            if ($this->contentForm->isValid()) {
                try {
                    $this->contentRepository->persist($content);
                    $this->flashMessenger()->addSuccessMessage(_('Content has been created'));

                    return $this->redirect()->toRoute('admin-content/edit', ['id' => $content->getId()]);
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(_('An error has occurred'));

                    return $this->redirect()->toRoute('admin-content/create');
                }
            }
        }

        return [
            'form' => $this->contentForm
        ];
    }

    public function editAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');

        $content = $this->contentRepository->getById($id);
        $this->contentForm->bind($content);

        if ($request->isPost()) {
            $this->contentForm->setData($request->getPost());

            if ($this->contentForm->isValid()) {
                try {
                    $this->contentRepository->persist($content);
                    $this->flashMessenger()->addSuccessMessage(_('Content has been created'));
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(_('An error has occurred'));
                }

                return $this->redirect()->toRoute('admin-content/edit', ['id' => $content->getId()]);
            }
        }

        return [
            'form' => $this->contentForm
        ];
    }

    public function deleteAction()
    {
        $id = $this->params()->fromRoute('id');
        $content = $this->contentRepository->getById($id);

        if ($content) {
            try {
                $this->contentRepository->remove($content);
                $this->flashMessenger()->addSuccessMessage(_('Content has been deleted'));
            } catch (Exception $e) {
                $this->flashMessenger()->addErrorMessage(_('An error has occurred'));
            }
        } else {
            $this->flashMessenger()->addErrorMessage(sprintf(_('The content with ID \'%s\' not found'), $id));
        }

        return $this->redirect()->toRoute('admin-content');
    }
}
