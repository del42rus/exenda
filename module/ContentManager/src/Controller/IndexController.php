<?php

namespace ContentManager\Controller;

use ContentManager\Domain\Repository\ContentRepositoryInterface;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
    private $contentRepository;

    public function __construct(ContentRepositoryInterface $contentRepository)
    {
        $this->contentRepository = $contentRepository;
    }

    public function showAction()
    {
        $id = $this->params()->fromRoute('id');

        $content = $this->contentRepository->getById($id);

        if (!$content) {
            $this->getResponse()->setStatusCode(404);
            return [];
        }

        return [
            'content' => $content
        ];
    }
}