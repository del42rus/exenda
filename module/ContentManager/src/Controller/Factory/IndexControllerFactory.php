<?php

namespace ContentManager\Controller\Factory;

use ContentManager\Controller\IndexController;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $contentRepository = $container->get('ContentRepository');

        return new IndexController($contentRepository);
    }
}