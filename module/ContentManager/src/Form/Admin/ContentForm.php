<?php

namespace ContentManager\Form\Admin;

use Zend\Form\Element\Submit;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;

class ContentForm extends Form
{
    public function init()
    {
        $this->add([
            'name' => 'title',
            'options' => [
                'label' => _('Title'),
            ],
            'attributes' => [
                'type' => 'text',
            ]
        ]);

        $this->add([
            'name' => 'text',
            'type' => Textarea::class,
            'options' => [
                'label' => _('Text'),
            ],
        ]);

        $this->add([
            'name' => 'id',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $this->add([
            'type' => Submit::class,
            'name' => 'submit',
            'attributes' => [
                'value' => _('Submit'),
            ]
        ]);
    }
}