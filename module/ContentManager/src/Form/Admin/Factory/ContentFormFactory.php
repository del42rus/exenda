<?php

namespace ContentManager\Form\Admin\Factory;

use ContentManager\InputFilter\Admin\ContentInputFilter;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Interop\Container\ContainerInterface;
use ContentManager\Form\Admin\ContentForm;
use Zend\ServiceManager\Factory\FactoryInterface;

class ContentFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new ContentForm();
        $form->setHydrator(new DoctrineObject($container->get('doctrine.entitymanager.orm_default')));
        $form->setInputFilter($container->get('InputFilterManager')->get(ContentInputFilter::class));

        return $form;
    }
}