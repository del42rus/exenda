<?php

namespace ContentManager\Persistence\Doctrine\Repository\Factory;

use ContentManager\Persistence\Doctrine\Repository\ContentRepository;
use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class ContentRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new ContentRepository($entityManager);
    }
}