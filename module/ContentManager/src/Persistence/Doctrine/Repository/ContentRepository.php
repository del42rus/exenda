<?php

namespace ContentManager\Persistence\Doctrine\Repository;

use ContentManager\Domain\Entity\Content;
use ContentManager\Domain\Repository\ContentRepositoryInterface;
use Core\Persistence\Doctrine\Repository\AbstractRepository;
use Zend\Paginator\Paginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

class ContentRepository extends AbstractRepository implements ContentRepositoryInterface
{
    protected $entityClass = Content::class;

    public function paginate($page)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $query = $queryBuilder->select('c')
            ->from($this->entityClass, 'c');

        $adapter = new DoctrineAdapter(new ORMPaginator($query));
        $paginator = new Paginator($adapter);

        $paginator->setCurrentPageNumber((int) $page);

        return $paginator;
    }
}