<?php

namespace ContentManager;

use ContentManager\Controller\Admin\Factory\ContentControllerFactory;
use ContentManager\Controller\Factory\IndexControllerFactory;
use ContentManager\Form\Admin\Factory\ContentFormFactory;
use ContentManager\Persistence\Doctrine\Repository\Factory\ContentRepositoryFactory;
use Doctrine\ORM\Mapping\Driver\YamlDriver;
use ContentManager\InputFilter\Admin\ContentInputFilter;
use ContentManager\Form\Admin\ContentForm;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => IndexControllerFactory::class,
            Controller\Admin\ContentController::class => ContentControllerFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'content-show' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/content/:id',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'show'
                    ],
                    'constraints' => [
                        'id' => '\d+',
                    ],
                ]
            ],
            'admin-content' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/admin/content',
                    'defaults' => [
                        'controller' => Controller\Admin\ContentController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'paginator' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/page/:page',
                            'constraints' => [
                                'page' => '\d+',
                            ],
                            'defaults' => [
                                'page' => 1,
                            ]
                        ]
                    ],
                    'create' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/create',
                            'defaults' => [
                                'action' => 'create',
                            ],
                        ],
                    ],
                    'edit' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/edit/:id',
                            'constraints' => [
                                'id' => '\d+',
                            ],
                            'defaults' => [
                                'action' => 'edit'
                            ]
                        ],
                    ],
                    'delete' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/delete/:id',
                            'constraints' => [
                                'id' => '\d+',
                            ],
                            'defaults' => [
                                'action' => 'delete'
                            ]
                        ],
                    ],
                ]
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            'ContentRepository' => ContentRepositoryFactory::class
        ],
    ],
    'form_elements' => [
        'factories' => [
            ContentForm::class => ContentFormFactory::class,
        ]
    ],
    'input_filters' => [
        'factories' => [
            ContentInputFilter::class => InvokableFactory::class
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __NAMESPACE__ => __DIR__ . '/../view',
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => YamlDriver::class,
                'cache' => 'array',
                'extension' => '.dcm.yml',
                'paths' => [__DIR__ . '/yml']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
    ],
];
