<?php

namespace Users;

use Users\Domain\Entity\User;
use Users\Form\Admin\Factory\UserFormFactory;
use Users\Form\Admin\UserForm;
use Users\Form\Common\Factory\LoginFormFactory;
use Users\Form\Common\LoginForm;
use Users\InputFilter\Admin\Factory\UserInputFilterFactory;
use Users\InputFilter\Common\LoginInputFilter;
use Users\InputFilter\Admin\UserInputFilter;
use Users\Persistence\Doctrine\Repository\Factory\UserRepositoryFactory;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\Factory\InvokableFactory;
use Doctrine\ORM\Mapping\Driver\YamlDriver;

return [
    'router' => [
        'routes' => [
            'admin-login' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/admin/login',
                    'defaults' => [
                        'controller' => Controller\Admin\LoginController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'admin-logout' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/admin/logout',
                    'defaults' => [
                        'controller' => Controller\Admin\LoginController::class,
                        'action' => 'logout',
                    ],
                ],
            ],
            'admin-users' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/admin/users',
                    'defaults' => [
                        'controller' => Controller\Admin\UserController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'paginator' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/page/:page',
                            'constraints' => [
                                'page' => '\d+',
                            ],
                            'defaults' => [
                                'page' => 1,
                            ]
                        ]
                    ],
                    'create' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/create',
                            'defaults' => [
                                'action' => 'create',
                            ],
                        ],
                    ],
                    'edit' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/edit/:id',
                            'constraints' => [
                                'id' => '\d+',
                            ],
                            'defaults' => [
                                'action' => 'edit'
                            ]
                        ],
                    ],
                    'delete' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/delete/:id',
                            'constraints' => [
                                'id' => '\d+',
                            ],
                            'defaults' => [
                                'action' => 'delete'
                            ]
                        ],
                    ],
                ]
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\Admin\LoginController::class => Controller\Admin\Factory\LoginControllerFactory::class,
            Controller\Admin\UserController::class => Controller\Admin\Factory\UserControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __NAMESPACE__ => __DIR__ . '/../view',
        ],
    ],
    'service_manager' => [
        'factories' => [
            'UserRepository' => UserRepositoryFactory::class
        ],
        'aliases' => [
            AuthenticationService::class => 'doctrine.authenticationservice.orm_default',
        ]
    ],
    'form_elements' => [
        'factories' => [
            UserForm::class => UserFormFactory::class,
            LoginForm::class => LoginFormFactory::class
        ]
    ],
    'input_filters' => [
        'factories' => [
            UserInputFilter::class => UserInputFilterFactory::class,
            LoginInputFilter::class => InvokableFactory::class
        ]
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => YamlDriver::class,
                'cache' => 'array',
                'extension' => '.dcm.yml',
                'paths' => [__DIR__ . '/yml']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver'
                ]
            ]
        ],

        'authentication' => [
            'orm_default' => [
                'objectManager' => 'doctrine.entitymanager.orm_default',
                'identityClass' => User::class,
                'identityProperty' => 'email',
                'credentialProperty' => 'password',
                'credentialCallable' => function($identity, $credentialValue) {
                    return password_verify($credentialValue, $identity->getPassword());
                }
            ],
        ],
    ],
];
