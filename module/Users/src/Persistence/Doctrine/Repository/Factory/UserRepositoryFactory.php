<?php

namespace Users\Persistence\Doctrine\Repository\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Users\Persistence\Doctrine\Repository\UserRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new UserRepository($entityManager);
    }
}