<?php

namespace Users\Persistence\Doctrine\Repository;

use Core\Persistence\Doctrine\Repository\AbstractRepository;
use Users\Domain\Entity\User;
use Users\Domain\Repository\UserRepositoryInterface;
use Zend\Paginator\Paginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

class UserRepository extends AbstractRepository implements UserRepositoryInterface
{
    protected $entityClass = User::class;

    public function paginate($page)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $query = $queryBuilder->select('u')
            ->from($this->entityClass, 'u');

        $adapter = new DoctrineAdapter(new ORMPaginator($query));
        $paginator = new Paginator($adapter);

        $paginator->setCurrentPageNumber((int) $page);

        return $paginator;
    }
}

