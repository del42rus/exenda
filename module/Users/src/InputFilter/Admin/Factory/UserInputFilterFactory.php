<?php

namespace Users\InputFilter\Admin\Factory;

use Interop\Container\ContainerInterface;
use Users\InputFilter\Admin\UserInputFilter;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserInputFilterFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $repository = $container->get('UserRepository');

        return new UserInputFilter($repository);
    }
}