<?php

namespace Users\InputFilter\Admin;

use Core\Domain\Repository\RepositoryInterface;
use Core\Validator\Unique;
use Users\Validator\PasswordConfirmation;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\InputFilter\InputFilter;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

class UserInputFilter extends InputFilter
{
    private $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function init()
    {
        $this->add([
            'name' => 'name',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'break_chain_on_failure' => true,
                    'name' => NotEmpty::class,
                ],
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 100,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => 'email',
            'filters'  => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'name' => Unique::class,
                    'options' => [
                        'attribute' => 'email',
                        'repository' => $this->repository
                    ]
                ],
            ],
        ]);

        $this->add([
            'name' => 'password',
            'required' => true,
            'filters'  => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'break_chain_on_failure' => true,
                    'name' => NotEmpty::class,
                ],
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 6,
                    ],
                ],
            ],
        ]);

        $this->add([
            'name' => 'password_confirmation',
            'required' => true,
            'filters'  => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'break_chain_on_failure' => true,
                    'name' => NotEmpty::class,
                ],
                [
                    'name' => PasswordConfirmation::class,
                ]
            ],
        ]);
    }
}