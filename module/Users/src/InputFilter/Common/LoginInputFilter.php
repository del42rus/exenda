<?php

namespace Users\InputFilter\Common;

use Core\Domain\Repository\RepositoryInterface;
use Core\Validator\Unique;
use Users\Validator\PasswordConfirmation;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\InputFilter\InputFilter;
use Zend\Validator\NotEmpty;
use Zend\Validator\StringLength;

class LoginInputFilter extends InputFilter
{
    public function init()
    {
        $this->add([
            'name' => 'email',
            'filters'  => [
                ['name' => StringTrim::class],
            ],
        ]);

        $this->add([
            'name' => 'password',
            'required' => true,
            'filters'  => [
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'break_chain_on_failure' => true,
                    'name' => NotEmpty::class,
                ],
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 6,
                    ],
                ],
            ],
        ]);
    }
}