<?php

namespace Users\Listener;

use Zend\Authentication\AuthenticationService;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\MvcEvent;
use Zend\Router\RouteMatch;

class AuthListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH, [$this, 'onDispatch'], 10000);
    }

    public function onDispatch(MvcEvent $e)
    {
        $matches = $e->getRouteMatch();

        if (!$matches instanceof RouteMatch) {
            return;
        }

        if (strpos($matches->getMatchedRouteName(), 'admin') === false ||
            in_array($matches->getMatchedRouteName(), ['admin-login', 'admin-logout'])
        ) {
            return;
        }

        $serviceLocator = $e->getApplication()->getServiceManager();
        $authService = $serviceLocator->get(AuthenticationService::class);

        if (!$authService->hasIdentity()) {
            $response = $e->getResponse();

            $response->getHeaders()->addHeaderLine('Location', '/admin/login');
            $response->setStatusCode(302);

            $e->stopPropagation();
            $e->setResponse($response);
            $e->setResult($response);
        }
    }
}
