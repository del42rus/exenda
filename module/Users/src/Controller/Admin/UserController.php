<?php

namespace Users\Controller\Admin;

use Exception;
use Users\Domain\Entity\User;
use Users\Domain\Repository\UserRepositoryInterface;
use Users\Form\Admin\UserForm;
use Zend\Mvc\Controller\AbstractActionController;

class UserController extends AbstractActionController
{
    private $userRepository;

    private $userForm;

    public function __construct(UserRepositoryInterface $userRepository, UserForm $userForm)
    {
        $this->userRepository = $userRepository;
        $this->userForm = $userForm;
    }

    public function indexAction()
    {
        $page = $this->params()->fromRoute('page');
        $users = $this->userRepository->paginate($page);

        return [
            'users' => $users
        ];
    }

    public function createAction()
    {
        $request = $this->getRequest();

        $user = new User();
        $this->userForm->bind($user);

        if ($request->isPost()) {
            $this->userForm->setData($request->getPost());

            if ($this->userForm->isValid()) {
                try {
                    $this->userRepository->persist($user);
                    $this->flashMessenger()->addSuccessMessage(_('User has been created'));

                    return $this->redirect()->toRoute('admin-users/edit', ['id' => $user->getId()]);
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(_('An error has occurred'));

                    return $this->redirect()->toRoute('admin-users/create');
                }
            }
        }

        return [
            'form' => $this->userForm
        ];
    }

    public function editAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');

        $user = $this->userRepository->getById($id);
        $this->userForm->bind($user);

        if ($request->isPost()) {
            $this->userForm->setData($request->getPost());

            if (!$request->getPost('password_confirmation') && !$request->getPost('password')) {
                $this->userForm->getInputFilter()->remove('password');
                $this->userForm->getInputFilter()->remove('password_confirmation');
            }

            if ($this->userForm->isValid()) {
                try {
                    $this->userRepository->persist($user);
                    $this->flashMessenger()->addSuccessMessage(_('User has been updated'));
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(_('An error has occurred'));
                }

                return $this->redirect()->toRoute('admin-users/edit', ['id' => $user->getId()]);
            }
        }

        return [
            'user' => $user,
            'form' => $this->userForm
        ];
    }

    public function deleteAction()
    {
        $id = $this->params()->fromRoute('id');

        $user = $this->userRepository->getById($id);

        if ($user) {
            try {
                $this->userRepository->remove($user);
                $this->flashMessenger()->addSuccessMessage(_('User has been deleted'));
            } catch (Exception $e) {
                $this->flashMessenger()->addErrorMessage(_('An error has occurred'));
            }
        } else {
            $this->flashMessenger()->addErrorMessage(sprintf(_('The user with ID \'%s\' not found'), $id));
        }

        return $this->redirect()->toRoute('admin-users');
    }
}
