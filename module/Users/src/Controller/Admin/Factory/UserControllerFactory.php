<?php

namespace Users\Controller\Admin\Factory;

use Interop\Container\ContainerInterface;
use Users\Controller\Admin\UserController;
use Users\Form\Admin\UserForm;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $userRepository = $container->get('UserRepository');
        $userForm = $container->get('FormElementManager')->get(UserForm::class);

        return new UserController($userRepository, $userForm);
    }
}