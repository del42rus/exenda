<?php

namespace Users\Controller\Admin\Factory;

use Interop\Container\ContainerInterface;
use Users\Controller\Admin\LoginController;
use Users\Form\Common\LoginForm;
use Zend\Authentication\AuthenticationService;
use Zend\ServiceManager\Factory\FactoryInterface;

class LoginControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $loginForm = $container->get('FormElementManager')->get(LoginForm::class);
        $authService = $container->get(AuthenticationService::class);

        return new LoginController($loginForm, $authService);
    }
}