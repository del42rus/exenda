<?php

namespace Users\Controller\Admin;

use DoctrineModule\Authentication\Adapter\ObjectRepository;
use Users\Form\Common\LoginForm;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\AuthenticationServiceInterface;
use Zend\Mvc\Controller\AbstractActionController;

class LoginController extends AbstractActionController
{
    private $loginForm;

    /** @var AuthenticationService */
    private $authService;

    public function __construct(LoginForm $loginForm, AuthenticationServiceInterface $authService)
    {
        $this->loginForm = $loginForm;
        $this->authService = $authService;
    }

    public function indexAction()
    {
        if ($this->authService->hasIdentity()) {
            return $this->redirect()->toRoute('admin');
        }

        $request = $this->getRequest();

        if ($request->isPost()) {
            $this->loginForm->setData($request->getPost());

            if ($this->loginForm->isValid()) {
                /** @var ObjectRepository $adapter */
                $adapter = $this->authService->getAdapter();

                $adapter->setIdentity($request->getPost('email'));
                $adapter->setCredential($request->getPost('password'));

                $authResult = $this->authService->authenticate();

                if ($authResult->isValid()) {
                    return $this->redirect()->toRoute('admin');
                } else {
                    return [
                        'form' => $this->loginForm,
                        'error' => _('Your authentication credentials are not valid')
                    ];
                }
            }
        }

        $this->layout()->setTemplate('layout/admin/login');

        return [
            'form' => $this->loginForm,
        ];
    }

    public function logoutAction()
    {
        $this->authService->clearIdentity();

        return $this->redirect()->toRoute('admin-login');
    }
}
