<?php

namespace Users\Validator;

use Zend\Validator\AbstractValidator;

class PasswordConfirmation extends AbstractValidator
{
    const NOT_MATCH = 'notMatch';

    protected $messageTemplates = [
        self::NOT_MATCH => 'Passwords do not match'
    ];

    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        if (is_array($context)) {
            if ($value == $context['password']) {
                return true;
            }
        } elseif (is_string($context) && ($value == $context)) {
            return true;
        }

        $this->error(self::NOT_MATCH);

        return false;
    }
}