<?php

namespace Users\Form\Common\Factory;

use Interop\Container\ContainerInterface;
use Users\Form\Common\LoginForm;
use Users\InputFilter\Common\LoginInputFilter;
use Zend\ServiceManager\Factory\FactoryInterface;

class LoginFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new LoginForm();
        $form->setInputFilter($container->get('InputFilterManager')->get(LoginInputFilter::class));

        return $form;
    }
}