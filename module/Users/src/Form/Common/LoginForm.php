<?php

namespace Users\Form\Common;

use Zend\Form\Element\Email;
use Zend\Form\Element\Password;
use Zend\Form\Element\Submit;
use Zend\Form\Form;

class LoginForm extends Form
{
    public function init()
    {
        $this->add([
            'name' => 'email',
            'type' => Email::class,
            'options' => [
                'label' => _('Email'),
            ],
        ]);

        $this->add([
            'type' => Password::class,
            'name' => 'password',
            'options' => [
                'label' => _('Password'),
            ],
            'attributes' => [
                'autocomplete' => 'off'
            ]
        ]);

        $this->add([
            'type' => Submit::class,
            'name' => 'submit',
            'attributes' => [
                'value' => _('Submit'),
            ]
        ]);
    }
}