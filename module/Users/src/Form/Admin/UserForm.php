<?php

namespace Users\Form\Admin;

use Zend\Form\Element\Email;
use Zend\Form\Element\Password;
use Zend\Form\Element\Submit;
use Zend\Form\Form;

class UserForm extends Form
{
    public function init()
    {
        $this->add([
            'name' => 'name',
            'options' => [
                'label' => _('Name'),
            ],
            'attributes' => [
                'type' => 'text',
            ]
        ]);

        $this->add([
            'type' => Email::class,
            'name' => 'email',
            'options' => [
                'label' => _('Email'),
            ],
        ]);

        $this->add([
            'type' => Password::class,
            'name' => 'password',
            'options' => [
                'label' => _('Password'),
            ],
            'attributes' => [
                'autocomplete' => 'off'
            ]
        ]);

        $this->add([
            'type' => Password::class,
            'name' => 'password_confirmation',
            'options' => [
                'label' => _('Confirm Password'),
            ],
        ]);

        $this->add([
            'name' => 'id',
            'attributes' => [
                'type' => 'hidden',
            ],
        ]);

        $this->add([
            'type' => Submit::class,
            'name' => 'submit',
            'attributes' => [
                'value' => _('Submit'),
            ]
        ]);
    }
}