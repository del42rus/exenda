<?php

namespace Users\Form\Admin\Factory;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Interop\Container\ContainerInterface;
use Users\Form\Admin\UserForm;
use Users\InputFilter\Admin\UserInputFilter;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new UserForm();
        $form->setHydrator(new DoctrineObject($container->get('doctrine.entitymanager.orm_default')));
        $form->setInputFilter($container->get('InputFilterManager')->get(UserInputFilter::class));

        return $form;
    }
}