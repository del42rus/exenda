<?php

namespace MenuManager\Form\Admin;

use DoctrineModule\Persistence\ProvidesObjectManager;
use MenuManager\Domain\Entity\MenuItem;
use MenuManager\Domain\Repository\MenuItemRepositoryInterface;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\Form\Element\Submit;
use Zend\Form\Form;

class MenuItemForm extends Form
{
    private $menuId;

    private $menuItem;

    private $menuItemRepository;

    public function __construct(MenuItemRepositoryInterface $menuItemRepository, $menuId, $menuItem)
    {
        $this->menuItemRepository = $menuItemRepository;
        $this->menuId = $menuId;
        $this->menuItem = $menuItem;

        parent::__construct();
    }

    public function init()
    {
        $menuItems = $this->menuItemRepository->getBy(['menu' => $this->menuId]);
        $valueOptions = [];

        foreach ($menuItems as $menuItem) {
            if ($this->menuItem && ($menuItem->getId() == $this->menuItem->getId() ||
                in_array($menuItem->getId(), $this->menuItem->getChildrenIds()))
            ) {
                continue;
            }
            $valueOptions[$menuItem->getId()] = $menuItem->getTitle();
        }

        $this->add([
            'type' => Select::class,
            'name' => 'parent',
            'options' => [
                'label' => _('Parent'),
                'empty_option' => _('None'),
                'value_options' => $valueOptions
            ],
        ]);

        $this->add([
            'name' => 'title',
            'options' => [
                'label' => _('Title'),
            ],
            'attributes' => [
                'type' => 'text',
            ]
        ]);

        $this->add([
            'name' => 'alias',
            'options' => [
                'label' => _('Alias'),
            ],
            'attributes' => [
                'type' => 'text',
            ]
        ]);

        $this->add([
            'name' => 'route',
            'options' => [
                'label' => _('Route'),
            ],
            'attributes' => [
                'type' => 'text',
            ]
        ]);

        $this->add([
            'name' => 'id',
            'type' => Hidden::class,
        ]);

        $this->add([
            'type' => Submit::class,
            'name' => 'submit',
            'attributes' => [
                'value' => _('Submit'),
            ]
        ]);
    }
}