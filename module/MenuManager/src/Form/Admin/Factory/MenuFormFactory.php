<?php

namespace MenuManager\Form\Admin\Factory;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Interop\Container\ContainerInterface;
use MenuManager\Form\Admin\MenuForm;
use MenuManager\InputFilter\Admin\MenuInputFilter;
use Zend\ServiceManager\Factory\FactoryInterface;

class MenuFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new MenuForm();
        $form->setHydrator(new DoctrineObject($container->get('doctrine.entitymanager.orm_default')));
        $form->setInputFilter($container->get('InputFilterManager')->get(MenuInputFilter::class));

        return $form;
    }
}