<?php

namespace MenuManager\Form\Admin\Factory;

use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Interop\Container\ContainerInterface;
use MenuManager\Form\Admin\MenuItemForm;
use MenuManager\InputFilter\Admin\MenuItemInputFilter;
use Zend\ServiceManager\Factory\FactoryInterface;

class MenuItemFormFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $menuItemRepository = $container->get('MenuItemRepository');

        $app = $container->get('Application');
        $mvcEvent = $app->getMvcEvent();
        $routeMatch = $mvcEvent->getRouteMatch();

        $menuId = $routeMatch->getParam('menuId');
        $menuItemId = $routeMatch->getParam('id');

        $menuItem = null;
        if ($menuItemId) {
            $menuItem = $menuItemRepository->getById($menuItemId);
        }

        $form = new MenuItemForm($menuItemRepository, $menuId, $menuItem);
        $form->setHydrator(new DoctrineObject($container->get('doctrine.entitymanager.orm_default'), false));
        $form->setInputFilter($container->get('InputFilterManager')->get(MenuItemInputFilter::class));

        return $form;
    }
}