<?php

namespace MenuManager\Form\Admin;

use Zend\Form\Element\Submit;
use Zend\Form\Element\Textarea;
use Zend\Form\Form;

class MenuForm extends Form
{
    public function init()
    {
        $this->add([
            'name' => 'name',
            'options' => [
                'label' => _('Name'),
            ],
            'attributes' => [
                'type' => 'text',
            ]
        ]);

        $this->add([
            'name' => 'identifier',
            'options' => [
                'label' => _('Name'),
            ],
            'attributes' => [
                'type' => 'text',
            ]
        ]);

        $this->add([
            'name' => 'description',
            'type' => Textarea::class,
            'options' => [
                'label' => _('Description'),
            ],
        ]);

        $this->add([
            'type' => Submit::class,
            'name' => 'submit',
            'attributes' => [
                'value' => _('Submit'),
            ]
        ]);
    }
}