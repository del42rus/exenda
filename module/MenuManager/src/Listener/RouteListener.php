<?php

namespace MenuManager\Listener;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\MvcEvent;
use Zend\Router\RouteMatch;

class RouteListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, [$this, 'onRoute'], $priority);
    }

    public function onRoute(MvcEvent $event)
    {
        $request = $event->getRequest();
        $router     = $event->getRouter();
        $routeMatch = $router->match($request);

        if ($routeMatch instanceof RouteMatch) {
            $event->setRouteMatch($routeMatch);
            return $routeMatch;
        }

        $app = $event->getApplication();
        $serviceManager = $app->getServiceManager();

        $menuItemRepository = $serviceManager->get('MenuItemRepository');

        $uri = $request->getUri();
        $path = trim($uri->getPath(), '/');
        $parts = explode('/', trim($path, '/'));

        $parent = null;

        foreach ($parts as $part) {
            $menuItem = $menuItemRepository->getByAlias($part, $parent);

            if (!$menuItem) {
                return;
            }

            $parent = $menuItem->getId();
        }

        $uri->setPath($menuItem->getRoute());
        $request->setUri($uri);

        $event->setRequest($request);
    }
}