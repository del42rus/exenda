<?php

namespace MenuManager\Controller\Admin;

use Exception;
use MenuManager\Domain\Entity\Menu;
use MenuManager\Domain\Repository\MenuRepositoryInterface;
use MenuManager\Form\Admin\MenuForm;
use Zend\Mvc\Controller\AbstractActionController;

class MenuController extends AbstractActionController
{
    private $menuRepository;

    private $menuForm;

    public function __construct(MenuRepositoryInterface $menuRepository, MenuForm $menuForm)
    {
        $this->menuRepository = $menuRepository;
        $this->menuForm = $menuForm;
    }

    public function indexAction()
    {
        $menus = $this->menuRepository->getAll();

        return [
            'menus' => $menus
        ];
    }

    public function createAction()
    {
        $request = $this->getRequest();

        $menu = new Menu();
        $this->menuForm->bind($menu);

        if ($request->isPost()) {
            $this->menuForm->setData($request->getPost());

            if ($this->menuForm->isValid()) {
                try {
                    $this->menuRepository->persist($menu);
                    $this->flashMessenger()->addSuccessMessage(_('Menu has been created'));

                    return $this->redirect()->toRoute('admin-menus/edit', ['id' => $menu->getId()]);
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(_('An error has occurred'));

                    return $this->redirect()->toRoute('admin-menus/create');
                }
            }
        }

        return [
            'form' => $this->menuForm
        ];
    }

    public function editAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');

        $menu = $this->menuRepository->getById($id);
        $this->menuForm->bind($menu);

        if ($request->isPost()) {
            $this->menuForm->setData($request->getPost());

            if ($this->menuForm->isValid()) {
                try {
                    $this->menuRepository->persist($menu);
                    $this->flashMessenger()->addSuccessMessage(_('Menu has been created'));

                    return $this->redirect()->toRoute('admin-menus/edit', ['id' => $menu->getId()]);
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(_('An error has occurred'));

                    return $this->redirect()->toRoute('admin-menus/create');
                }
            }
        }

        return [
            'form' => $this->menuForm
        ];
    }

    public function deleteAction()
    {
        $id = $this->params()->fromRoute('id');
        $menu = $this->menuRepository->getById($id);

        if ($menu) {
            try {
                $this->menuRepository->remove($menu);
                $this->flashMessenger()->addSuccessMessage(_('Menu has been deleted'));
            } catch (Exception $e) {
                $this->flashMessenger()->addErrorMessage(_('An error has occurred'));
            }
        } else {
            $this->flashMessenger()->addErrorMessage(sprintf(_('Menu with ID \'%s\' not found'), $id));
        }

        return $this->redirect()->toRoute('admin-menus');
    }
}
