<?php

namespace MenuManager\Controller\Admin\Factory;

use Interop\Container\ContainerInterface;
use MenuManager\Controller\Admin\MenuItemController;
use MenuManager\Form\Admin\MenuItemForm;
use Zend\ServiceManager\Factory\FactoryInterface;

class MenuItemControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $menuItemRepository = $container->get('MenuItemRepository');
        $menuRepository = $container->get('MenuRepository');
        $menuItemForm = $container->get('FormElementManager')->get(MenuItemForm::class);

        return new MenuItemController($menuRepository, $menuItemRepository, $menuItemForm);
    }
}