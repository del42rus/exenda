<?php

namespace MenuManager\Controller\Admin\Factory;

use Interop\Container\ContainerInterface;
use MenuManager\Controller\Admin\MenuController;
use MenuManager\Form\Admin\MenuForm;
use MenuManager\Persistence\Doctrine\Repository\MenuRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class MenuControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $menuRepository = $container->get('MenuRepository');
        $menuForm = $container->get('FormElementManager')->get(MenuForm::class);

        return new MenuController($menuRepository, $menuForm);
    }
}