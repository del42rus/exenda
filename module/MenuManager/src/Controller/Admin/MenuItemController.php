<?php

namespace MenuManager\Controller\Admin;

use Exception;
use MenuManager\Domain\Entity\MenuItem;
use MenuManager\Domain\Repository\MenuItemRepositoryInterface;
use MenuManager\Domain\Repository\MenuRepositoryInterface;
use MenuManager\Form\Admin\MenuItemForm;
use Zend\Mvc\Controller\AbstractActionController;

class MenuItemController extends AbstractActionController
{
    private $menuRepository;

    private $menuItemRepository;

    private $menuItemForm;

    public function __construct(MenuRepositoryInterface $menuRepository,
                                MenuItemRepositoryInterface $menuItemRepository,
                                MenuItemForm $menuItemForm) {
        $this->menuRepository = $menuRepository;
        $this->menuItemRepository = $menuItemRepository;
        $this->menuItemForm = $menuItemForm;
    }

    public function indexAction()
    {
        $menuId = $this->params()->fromRoute('menuId');

        $menuItems = $this->menuItemRepository->getBy(['menu' => $menuId]);

        return [
            'menuId' => $menuId,
            'menuItems' => $menuItems
        ];
    }

    public function createAction()
    {
        $request = $this->getRequest();
        $menuId = $this->params()->fromRoute('menuId');

        $menu = $this->menuRepository->getById($menuId);

        if (!$menu) {
            $this->getResponse()->setStatusCode(404);
            return [];
        }

        $menuItem = new MenuItem();
        $menuItem->setMenu($menu);
        $this->menuItemForm->bind($menuItem);

        if ($request->isPost()) {
            $this->menuItemForm->setData($request->getPost());

            if ($this->menuItemForm->isValid()) {
                try {
                    $this->menuItemRepository->persist($menuItem);
                    $this->flashMessenger()->addSuccessMessage(_('Menu item has been created'));

                    return $this->redirect()->toRoute('admin-menus/items/edit', ['menuId' => $menuId, 'id' => $menuItem->getId()]);
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(_('An error has occurred'));

                    return $this->redirect()->toRoute('admin-menus/items/create', ['menuId' => $menuId]);
                }
            }
        }

        return [
            'form' => $this->menuItemForm
        ];
    }

    public function editAction()
    {
        $request = $this->getRequest();
        $menuId = $this->params()->fromRoute('menuId');
        $id = $this->params()->fromRoute('id');

        $menuItem = $this->menuItemRepository->getById($id);

        if ($menuItem->getMenu()->getId() != $menuId) {
            $this->getResponse()->setStatusCode(404);
            return [];
        }

        $this->menuItemForm->bind($menuItem);

        if ($request->isPost()) {
            $this->menuItemForm->setData($request->getPost());

            if ($this->menuItemForm->isValid()) {
                try {
                    $this->menuItemRepository->persist($menuItem);
                    $this->flashMessenger()->addSuccessMessage(_('Menu item has been updated'));
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(_('An error has occurred'));
                }

                return $this->redirect()->toRoute('admin-menus/items/edit', ['menuId' => $menuId, 'id' => $menuItem->getId()]);
            }
        }

        return [
            'form' => $this->menuItemForm
        ];
    }

    public function deleteAction()
    {
        $id = $this->params()->fromRoute('id');
        $menuId = $this->params()->fromRoute('menuId');
        $menuItem = $this->menuItemRepository->getById($id);

        if ($menuItem) {
            try {
                $this->menuItemRepository->remove($menuItem);
                $this->flashMessenger()->addSuccessMessage(_('Menu item has been deleted'));
            } catch (Exception $e) {
                $this->flashMessenger()->addErrorMessage(_('An error has occurred'));
            }
        } else {
            $this->flashMessenger()->addErrorMessage(sprintf(_('Menu item with ID \'%s\' not found'), $id));
        }

        return $this->redirect()->toRoute('admin-menus/items', ['menuId' => $menuId]);
    }
}
