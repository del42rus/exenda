<?php

namespace MenuManager\Validator;

use MenuManager\Domain\Repository\MenuItemRepositoryInterface;
use Zend\Validator\AbstractValidator;

class UniqueAlias extends AbstractValidator
{
    const ALIAS_TAKEN = 'aliasExists';

    protected $messageTemplates = [
        self::ALIAS_TAKEN => "Alias '%value%' has been already taken"
    ];

    protected $menuItemRepository;

    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        $menuItem = $this->menuItemRepository->getByAlias($value, $context['parent']);

        if (!$menuItem || $menuItem->getId() == $context['id']) {
            return true;
        }

        $this->error(self::ALIAS_TAKEN);

        return false;
    }

    public function getMenuItemRepository()
    {
        return $this->menuItemRepository;
    }

    public function setMenuItemRepository(MenuItemRepositoryInterface $menuItemRepository)
    {
        $this->menuItemRepository = $menuItemRepository;
    }
}