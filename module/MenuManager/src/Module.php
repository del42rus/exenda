<?php

namespace MenuManager;

use MenuManager\Listener\RouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $event)
    {
        $app = $event->getApplication();
        $eventManager = $app->getEventManager();

        $routeListener = new RouteListener();
        $routeListener->attach($eventManager, 100);
    }

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
