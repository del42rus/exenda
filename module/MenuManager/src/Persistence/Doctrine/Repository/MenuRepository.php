<?php

namespace MenuManager\Persistence\Doctrine\Repository;

use Core\Persistence\Doctrine\Repository\AbstractRepository;
use MenuManager\Domain\Entity\Menu;
use MenuManager\Domain\Repository\MenuRepositoryInterface;

class MenuRepository extends AbstractRepository implements MenuRepositoryInterface
{
    protected $entityClass = Menu::class;
}