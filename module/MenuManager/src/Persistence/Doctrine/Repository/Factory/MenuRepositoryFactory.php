<?php

namespace MenuManager\Persistence\Doctrine\Repository\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use MenuManager\Persistence\Doctrine\Repository\MenuRepository;
use Zend\ServiceManager\Factory\FactoryInterface;

class MenuRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        return new MenuRepository($entityManager);
    }
}