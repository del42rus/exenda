<?php

namespace MenuManager\Persistence\Doctrine\Repository;

use Core\Persistence\Doctrine\Repository\AbstractRepository;
use MenuManager\Domain\Entity\MenuItem;
use MenuManager\Domain\Repository\MenuItemRepositoryInterface;

class MenuItemRepository extends AbstractRepository implements MenuItemRepositoryInterface
{
    protected $entityClass = MenuItem::class;

    public function getByAlias($alias, $parent = null)
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder->select('i')
            ->from(MenuItem::class, 'i');

        $queryBuilder->where('i.alias = ?1');

        if (!$parent) {
            $queryBuilder->andWhere($queryBuilder->expr()->isNull('i.parent'));
        } else {
            $queryBuilder->andWhere('i.parent = ?2');
            $queryBuilder->setParameter(2, $parent);
        }

        $queryBuilder->setParameter(1, $alias);
        $query = $queryBuilder->getQuery();

        return $query->getOneOrNullResult();
    }
}