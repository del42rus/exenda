<?php

namespace MenuManager\Domain\Entity;

use Core\Domain\Entity\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;

class MenuItem extends AbstractEntity
{
    private $parent;

    private $children;

    private $menu;

    private $alias;

    private $title;

    private $route;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent(MenuItem $parent)
    {
        $this->parent = $parent;
        return $this;
    }

    public function removeParent()
    {
        $this->parent = null;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function addChild(MenuItem $child)
    {
        $this->children[] = $child;
    }

    public function getChildrenIds($recursive = true)
    {
        $ids = [];

        foreach ($this->children as $child) {
            $ids[] = $child->getId();

            if (true === $recursive) {
                $ids = array_merge($ids, $child->getChildrenIds(true));
            }
        }

        return $ids;
    }

    public function getMenu()
    {
        return $this->menu;
    }

    public function setMenu(Menu $menu)
    {
        $this->menu = $menu;
        return $this;
    }

    public function removeMenu()
    {
        $this->menu = null;
    }

    public function getAlias()
    {
        return $this->alias;
    }

    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getRoute()
    {
        return $this->route;
    }

    public function setRoute($route)
    {
        $this->route = $route;
        return $this;
    }

    public function getParents()
    {
        $items = [$this];

        $parent = $this->getParent();

        if (!$parent) {
            return $items;
        }

        return array_merge($items, $parent->getParents());
    }

    public function getPath()
    {
        $items = $this->getParents();

        if (count($items)) {
            $parts = [];
            foreach (array_reverse($items) as $item) {
                $parts[] = $item->getAlias();
            }
            $path = join('/', $parts);
        }

        return '/' . $path;
    }
}