<?php

namespace MenuManager\Domain\Entity;

use Core\Domain\Entity\AbstractEntity;
use Doctrine\Common\Collections\ArrayCollection;

class Menu extends AbstractEntity
{
    private $name;

    private $identifier;

    private $description;

    private $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getIdentifier()
    {
        return $this->identifier;
    }

    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function addItem(MenuItem $item)
    {
        $this->items->add($item);
        $item->setMenu($this);
    }

    public function removeItem(MenuItem $item)
    {
        $this->items->remove($item);
        $item->removeMenu();
    }

    public function getItems()
    {
        return $this->items;
    }
}