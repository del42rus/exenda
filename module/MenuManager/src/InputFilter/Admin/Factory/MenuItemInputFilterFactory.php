<?php

namespace MenuManager\InputFilter\Admin\Factory;

use Interop\Container\ContainerInterface;
use MenuManager\InputFilter\Admin\MenuItemInputFilter;
use Zend\ServiceManager\Factory\FactoryInterface;

class MenuItemInputFilterFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $menuItemRepository = $container->get('MenuItemRepository');

        return new MenuItemInputFilter($menuItemRepository);
    }
}