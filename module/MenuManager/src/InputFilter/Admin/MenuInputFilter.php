<?php

namespace MenuManager\InputFilter\Admin;

use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\InputFilter\InputFilter;
use Zend\Validator\NotEmpty;

class MenuInputFilter extends InputFilter
{
    public function init()
    {
        $this->add([
            'name' => 'name',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'break_chain_on_failure' => true,
                    'name' => NotEmpty::class,
                ],
            ],
        ]);

        $this->add([
            'name' => 'identifier',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'break_chain_on_failure' => true,
                    'name' => NotEmpty::class,
                ],
            ],
        ]);
    }
}