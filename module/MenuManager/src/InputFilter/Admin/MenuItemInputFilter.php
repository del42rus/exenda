<?php

namespace MenuManager\InputFilter\Admin;

use MenuManager\Domain\Repository\MenuItemRepositoryInterface;
use MenuManager\Validator\UniqueAlias;
use Zend\Filter\StringTrim;
use Zend\Filter\StripTags;
use Zend\InputFilter\InputFilter;
use Zend\Validator\NotEmpty;

class MenuItemInputFilter extends InputFilter
{
    private $menuItemRepository;

    public function __construct(MenuItemRepositoryInterface $menuItemRepository)
    {
        $this->menuItemRepository = $menuItemRepository;
    }

    public function init()
    {
        $this->add([
            'name' => 'parent',
            'required' => false,
        ]);

        $this->add([
            'name' => 'alias',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'break_chain_on_failure' => true,
                    'name' => NotEmpty::class,
                ],
                [
                    'name' => UniqueAlias::class,
                    'options' => [
                        'menuItemRepository' => $this->menuItemRepository
                    ]
                ]
            ],
        ]);

        $this->add([
            'name' => 'title',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'break_chain_on_failure' => true,
                    'name' => NotEmpty::class,
                ],
            ],
        ]);

        $this->add([
            'name' => 'route',
            'required' => true,
            'filters' => [
                ['name' => StripTags::class],
                ['name' => StringTrim::class],
            ],
            'validators' => [
                [
                    'break_chain_on_failure' => true,
                    'name' => NotEmpty::class,
                ],
            ],
        ]);
    }
}