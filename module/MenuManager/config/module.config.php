<?php

namespace MenuManager;

use Doctrine\ORM\Mapping\Driver\YamlDriver;
use MenuManager\Controller\Admin\Factory\MenuControllerFactory;
use MenuManager\Controller\Admin\Factory\MenuItemControllerFactory;
use MenuManager\Form\Admin\Factory\MenuFormFactory;
use MenuManager\Form\Admin\Factory\MenuItemFormFactory;
use MenuManager\Form\Admin\MenuForm;
use MenuManager\Form\Admin\MenuItemForm;
use MenuManager\InputFilter\Admin\Factory\MenuItemInputFilterFactory;
use MenuManager\InputFilter\Admin\MenuInputFilter;
use MenuManager\InputFilter\Admin\MenuItemInputFilter;
use MenuManager\Persistence\Doctrine\Repository\Factory\MenuItemRepositoryFactory;
use MenuManager\Persistence\Doctrine\Repository\Factory\MenuRepositoryFactory;
use MenuManager\Persistence\Doctrine\Repository\MenuItemRepository;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'admin-menus' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/admin/menus',
                    'defaults' => [
                        'controller' => Controller\Admin\MenuController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'create' => [
                        'type' => 'Literal',
                        'options' => [
                            'route' => '/create',
                            'defaults' => [
                                'action' => 'create',
                            ],
                        ],
                    ],
                    'edit' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/edit/:id',
                            'constraints' => [
                                'id' => '\d+',
                            ],
                            'defaults' => [
                                'action' => 'edit'
                            ]
                        ],
                    ],
                    'delete' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/delete/:id',
                            'constraints' => [
                                'id' => '\d+',
                            ],
                            'defaults' => [
                                'action' => 'delete'
                            ]
                        ],
                    ],
                    'items' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/:menuId',
                            'defaults' => [
                                'controller' => Controller\Admin\MenuItemController::class,
                                'action' => 'index',
                            ],
                            'constraints' => [
                                'id' => '\d+',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'create' => [
                                'type' => 'Literal',
                                'options' => [
                                    'route' => '/items/create',
                                    'defaults' => [
                                        'action' => 'create',
                                    ],
                                ],
                            ],
                            'edit' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route' => '/items/edit/:id',
                                    'constraints' => [
                                        'id' => '\d+',
                                    ],
                                    'defaults' => [
                                        'action' => 'edit'
                                    ]
                                ],
                            ],
                            'delete' => [
                                'type' => 'Segment',
                                'options' => [
                                    'route' => '/items/delete/:id',
                                    'constraints' => [
                                        'id' => '\d+',
                                    ],
                                    'defaults' => [
                                        'action' => 'delete'
                                    ]
                                ],
                            ],
                        ]
                    ]
                ]
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\Admin\MenuController::class => MenuControllerFactory::class,
            Controller\Admin\MenuItemController::class => MenuItemControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __NAMESPACE__ => __DIR__ . '/../view',
        ],
    ],
    'service_manager' => [
        'factories' => [
            'MenuRepository' => MenuRepositoryFactory::class,
            'MenuItemRepository' => MenuItemRepositoryFactory::class
        ],
    ],
    'form_elements' => [
        'factories' => [
            MenuForm::class => MenuFormFactory::class,
            MenuItemForm::class => MenuItemFormFactory::class
        ]
    ],
    'input_filters' => [
        'factories' => [
            MenuInputFilter::class => InvokableFactory::class,
            MenuItemInputFilter::class => MenuItemInputFilterFactory::class,
        ]
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => YamlDriver::class,
                'cache' => 'array',
                'extension' => '.dcm.yml',
                'paths' => [__DIR__ . '/yml']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
    ],
];
