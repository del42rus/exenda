<?php

namespace Core;

use Gedmo\Timestampable\TimestampableListener;
use Core\Form\View;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'doctrine' => [
        'eventmanager' => [
            'orm_default' => [
                'subscribers' => [
                    TimestampableListener::class
                ],
            ],
        ],
    ],

    'view_helpers' => [
        'aliases' => [
            'form_element_errors' => View\Helper\FormElementErrors::class,
            'formelementerrors' => View\Helper\FormElementErrors::class,
            'formElementErrors' => View\Helper\FormElementErrors::class,
            'FormElementErrors' => View\Helper\FormElementErrors::class,
        ],
        'factories' => [
            View\Helper\FormElementErrors::class  => InvokableFactory::class,
        ]
    ]
];
