<?php

namespace Core\Persistence\Doctrine\Repository;

use Core\Domain\Entity\AbstractEntity;
use Core\Domain\Repository\RepositoryInterface;
use Doctrine\ORM\EntityManager;

abstract class AbstractRepository implements RepositoryInterface
{
    protected $entityManager;

    protected $entityClass;

    public function __construct(EntityManager $entityManager)
    {
        if (empty($this->entityClass)) {
            throw new \RuntimeException(get_class($this) . '::$entityClass is not defined');
        }

        $this->entityManager = $entityManager;
    }

    public function getById($id)
    {
        return $this->entityManager->getRepository($this->entityClass)->find($id);
    }

    public function getAll()
    {
        return $this->entityManager->getRepository($this->entityClass)->findAll();
    }

    public function getBy($conditions = [], $order = [], $limit = null, $offset = null)
    {
        return $this->entityManager->getRepository($this->entityClass)->findBy($conditions, $order, $limit, $offset);
    }

    public function getOneBy($conditions = [], $order = [])
    {
        return $this->entityManager->getRepository($this->entityClass)->findOneBy($conditions, $order);
    }

    public function persist(AbstractEntity $entity)
    {
        if (!$entity->getId()) {
            $this->entityManager->persist($entity);
        }
        $this->entityManager->flush();

        return $this;
    }

    public function remove(AbstractEntity $entity)
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    public function begin()
    {
        $this->entityManager->beginTransaction();

        return $this;
    }

    public function commit()
    {
        $this->entityManager->commit();

        return $this;
    }
}