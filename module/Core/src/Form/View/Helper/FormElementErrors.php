<?php

namespace Core\Form\View\Helper;

use Zend\Form\View\Helper\FormElementErrors as ZendFormElementErrors;

class FormElementErrors extends ZendFormElementErrors
{
    protected $messageCloseString     = '</div>';
    protected $messageOpenFormat      = '<div class="invalid-feedback">';
    protected $messageSeparatorString = '</div><div class="invalid-feedback">';
}