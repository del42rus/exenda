<?php

namespace Core\Validator;

use Core\Persistence\Doctrine\Repository\AbstractRepository;
use Zend\Validator\AbstractValidator;

class Unique extends AbstractValidator
{
    const ATTRIBUTE_TAKEN = 'attributeTaken';

    protected $messageTemplates = [
        self::ATTRIBUTE_TAKEN =>  "The '%attribute%' has been already taken"
    ];

    protected $messageVariables = [
        'attribute' => 'attribute'
    ];

    protected $attribute;

    /**
     * @var AbstractRepository
     */
    protected $repository;

    public function __construct($options = null)
    {
        parent::__construct($options);
    }

    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        $entity = $this->repository->getOneBy([$this->attribute => $value]);

        if (null === $entity || $entity->getId() == $context['id']) {
            return true;
        }

        $this->error(self::ATTRIBUTE_TAKEN);
        return false;
    }

    /**
     * @return mixed
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * @param mixed $attribute
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * @return AbstractRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param AbstractRepository $repository
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;
    }
}