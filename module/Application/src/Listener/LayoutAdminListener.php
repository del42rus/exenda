<?php

namespace Application\Listener;

use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\MvcEvent;
use Zend\Router\RouteMatch;

class LayoutAdminListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    public function attach(EventManagerInterface $events, $priority = 1)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH, [$this, 'setLayout'], 100);
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH_ERROR, [$this, 'setLayout'], 100);
    }

    public function setLayout(MvcEvent $e)
    {
        $matches  = $e->getRouteMatch();

        if (!$matches instanceof RouteMatch) {
            return;
        }

        if (strpos($matches->getMatchedRouteName(), 'admin') !== 0) {
            return;
        }

        $viewModel = $e->getViewModel();
        $viewModel->setTemplate('layout/admin/layout');
    }
}