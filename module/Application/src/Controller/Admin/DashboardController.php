<?php

namespace Application\Controller\Admin;

use Zend\Mvc\Controller\AbstractActionController;

class DashboardController extends AbstractActionController
{
    public function indexAction()
    {
        return [];
    }
}