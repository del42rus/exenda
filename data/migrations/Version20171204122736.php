<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171204122736 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $menusTable = $schema->createTable('menus');
        $menusTable->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $menusTable->addColumn('name', 'string');
        $menusTable->addColumn('identifier', 'string');
        $menusTable->addColumn('description', 'text', ['notnull' => false]);
        $menusTable->setPrimaryKey(['id']);

        $menuItemsTable = $schema->createTable('menu_items');
        $menuItemsTable->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $menuItemsTable->addColumn('parent_id', 'integer', ['unsigned' => true, 'notnull' => false]);
        $menuItemsTable->addColumn('menu_id', 'integer', ['unsigned' => true, 'notnull' => false]);
        $menuItemsTable->addColumn('title', 'string');
        $menuItemsTable->addColumn('alias', 'string');
        $menuItemsTable->addColumn('route', 'string');
        $menuItemsTable->setPrimaryKey(['id']);
        $menuItemsTable->addForeignKeyConstraint($menusTable, ['menu_id'], ['id'], ['onDelete' => 'CASCADE', 'onUpdated' => 'CASCADE']);
        $menuItemsTable->addForeignKeyConstraint($menuItemsTable, ['parent_id'], ['id'], ['onDelete' => 'CASCADE', 'onUpdated' => 'CASCADE']);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('menu_items');
        $schema->dropTable('menus');
    }
}
